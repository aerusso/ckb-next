Description: Debianizing install path
 Adding cmake options to install systemd unit files, udev rules, and libexec
 to FreeDesktop.org paths instead of using the RedHat and Arch-based paths.
 .
 Udev rules changes were accepted upstream. This patch was updated to remove
 that part of the patch. systemd install location still needs this patch.
Author: Stewart Ferguson <stew@ferg.aero>
Forwarded: https://github.com/ckb-next/ckb-next/pull/369
Last-Update: 2019-11-02
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/daemon/CMakeLists.txt
+++ b/src/daemon/CMakeLists.txt
@@ -419,9 +419,14 @@ if ("${CKB_NEXT_INIT_SYSTEM}" STREQUAL "
         GROUP_READ
         WORLD_READ)
 elseif ("${CKB_NEXT_INIT_SYSTEM}" STREQUAL "systemd")
+    if (${DEBIANIZE_INSTALL_PATHS})
+      set( _SYSTEMD_PATH "/lib/systemd/system")
+    else()
+      set( _SYSTEMD_PATH "/usr/lib/systemd/system")
+    endif()
     install(
         FILES "${CMAKE_CURRENT_BINARY_DIR}/service/ckb-next-daemon.service"
-        DESTINATION "/usr/lib/systemd/system"
+        DESTINATION ${_SYSTEMD_PATH}
         PERMISSIONS
         OWNER_READ OWNER_WRITE
         GROUP_READ
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -126,6 +126,8 @@ if (MACOS)
 elseif (LINUX)
     set(INSTALL_DIR_ANIMATIONS "${CMAKE_INSTALL_LIBEXECDIR}/${ANIMATIONS_DIR_NAME}"
         CACHE STRING "Where to install animations.")
+    option(DEBIANIZE_INSTALL_PATHS "Install files according to Debian and
+        FreeDesktop.org policy" OFF)
 endif ()
 
 # Options: other
